
'use strict';

/* 
 * AngularJS Utils
 * 
 * Authors:
 *      + Ivan Dario Cuadro
 *        id.cuadro82@gmail.com
 * Date: 2017/11
 */

var appView = angular.module('app.directives.appView', []);

appView.directive('appView', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            templateUrl: 'libs/app.utils/directives/appView/appView.html',
            replace: true,
            transclude: true,
            scope: {
                appTitle: '=?',
                appViewLoading: '=?',
                appViewFluid: '=?'
            },
            link: {
                post: function postLink(scope, elem, attrs) {
                    // TODO
                }
            }
        };
    }]);

