'use strict';

app.controller("lcdCtrl", ["$scope", "$timeout", "appGenerateLcdOuput",
    function ($scope, $timeout, appGenerateLcdOuput) {

        //******************************************************************
        // CONSTANTS
        //******************************************************************

        //Ejemplo Entrada: 2,12345 3,67890 0,0
        //REGLAS
        //  Debe Finalizar con la cadena 0,0
        //  La Cadena 0,0 debe estar al final
        //  Excepto la del final, ninguna otra cadena empezaria en 0 
        var expreg = /^(([1-9]|[1-9][0-9])+[,][0-9]+\s?)+[0][,][0]+$/;
        var inputFinalWord = "0,0";

        //******************************************************************
        // VARIABLES
        //******************************************************************

        //Auxiliary Variables
        $scope.loadingView = false;//Load Animation

        //Main Variables
        $scope.inputString = "";//Input String
        $scope.outputWordImages = [];//Matrix Output


        //******************************************************************
        // LOGIC
        //******************************************************************

        /**
         * Validate that String Input is correct
         * @param {String} input 
         */
        var validateInputString = function (input) {
            if (expreg.test(input)) {
                return true;
            } else {
                alert($scope.lang.lcd.input_format_error);
                return false;
            }
        };


        $scope.generateOutputImage = function (input) {
            $scope.outputWordImages.length = 0;

            if (validateInputString(input)) {
                $scope.loadingView = true;

                //Separate the input into words
                var inputStringArray = stringToArrayBySpaces(input);//Located in js/functions.js

                $.each(inputStringArray, function (index, word) {
                    if (word != inputFinalWord) {

                        var wordParametersArray = stringToArrayByCommas(word);//Located in js/functions.js

                        var lcdParam = {};

                        var i;
                        for (i in wordParametersArray) {
                            // i=0 for size, i=1 for output string
                            switch (i) {
                                case "0":
                                    lcdParam.size = wordParametersArray[i];
                                    break;
                                case "1":
                                    lcdParam.word = wordParametersArray[i];
                                    break;
                                default:
                                    console.error('Error en la expresión regular');
                                    alert($scope.lang.globals.error);
                            }
                        }

                        $scope.outputWordImages.push(appGenerateLcdOuput(lcdParam));//Located in lcd.services.js
                    }
                });

                $scope.loadingView = false;
            }
        };


        //******************************************************************
        // LOAD JQUERY FUNCTIONS
        //******************************************************************

        /**
         * Load popover contents
         */
        $scope.loadPopover = function () {
            $timeout(function () {
                var content = $scope.lang.lcd.input_format;
                var placement = 'right';

                loadPopoverJquery(content, placement);//Located in js/functions.js
            }, 500);
        };

        $scope.loadPopover();
    }
]);

