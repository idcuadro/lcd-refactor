'use strict';

/**
 * This Factory create the word image.
 * @param {Object} {size: int, word: string}
 */
app.factory('appGenerateLcdOuput', ['$rootScope', 'appGenerateDigitMatrix',
    function ($rootScope, appGenerateDigitMatrix) {
        return function (params) {
            var columnSize = 0;
            var rowSize = 0;
            var digitsArray = [];
            var wordImage = [];

            if (typeof params.size != 'undefined') {
                rowSize = (1 * params.size) + 2;
                columnSize = 2 * params.size + 3;
            } else {
                console.error('Error en appGenerateLcdOuput param.size');
                alert($rootScope.lang.lcd.input_format_error);
                return false;
            }

            if (typeof params.word != 'undefined') {
                digitsArray = stringToArrayByCharacter(params.word);//Located in js/functions.js
            } else {
                console.error('Error en appGenerateLcdOuput params.word');
                alert($rootScope.lang.lcd.input_format_error);
                return false;
            }

            //Fill matrix digits
            $.each(digitsArray, function (index, digit) {
                var baseMatrix = [];//Base digit matrix

                for (var i = 0; i < columnSize; i++) {
                    baseMatrix[i] = new Array(rowSize);
                }

                wordImage.push(appGenerateDigitMatrix(digit, baseMatrix));
            });

            return wordImage;
        };
    }
]);


/**
 * Convert a digit into Matrix.
 * @param {Array}
 */
app.factory('appGenerateDigitMatrix', ["$rootScope",
    function ($rootScope) {
        var hCh = "___";//Horizontal Character
        var vCh = "|";// Vertical Character

        var fillTopRow = function (matrix) {
            for (var x = 1; x < matrix[0].length - 1; x++) {
                matrix[0][x] = hCh;
            }
        };

        var fillMediumRow = function (matrix) {
            var mediumRow = (matrix.length - 1) / 2;

            for (var x = 1; x < matrix[0].length - 1; x++) {
                matrix[mediumRow][x] = hCh;
            }
        };

        var fillBottomRow = function (matrix) {
            var lastRow = matrix.length - 1;

            for (var x = 1; x < matrix[0].length - 1; x++) {
                matrix[lastRow][x] = hCh;
            }
        };

        var fillRightTop = function (matrix) {
            var mediumRow = (matrix.length - 1) / 2;

            for (var x = 1; x < mediumRow; x++) {
                var lastColumn = matrix[x].length - 1;
                matrix[x][lastColumn] = vCh;
            }
        };

        var fillLeftTop = function (matrix) {
            var mediumRow = (matrix.length - 1) / 2;

            for (var x = 1; x < mediumRow; x++) {
                matrix[x][0] = vCh;
            }
        };

        var fillLeftBottom = function (matrix) {
            var mediumRow = (matrix.length - 1) / 2;

            for (var x = mediumRow + 1; x < matrix.length - 1; x++) {
                matrix[x][0] = vCh;
            }
        };

        var fillRightBottom = function (matrix) {
            var mediumRow = (matrix.length - 1) / 2;

            for (var x = mediumRow + 1; x < matrix.length - 1; x++) {
                var lastColumn = matrix[x].length - 1;
                matrix[x][lastColumn] = vCh;
            }
        };

        var fillEmptyElements = function (matrix) {
            for (var y = 0; y < matrix.length; y++) {
                for (var x = 0; x < matrix[y].length; x++) {
                    if (typeof matrix[y][x] == 'undefined') {
                        matrix[y][x] = "";
                    }
                }
            }
        };

        return function (digit, matrix) {
            switch (digit) {
                case "0":
                    fillTopRow(matrix);
                    fillLeftTop(matrix);
                    fillRightTop(matrix);
                    fillLeftBottom(matrix);
                    fillRightBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "1":
                    fillRightTop(matrix);
                    fillRightBottom(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "2":
                    fillTopRow(matrix);
                    fillRightTop(matrix);
                    fillMediumRow(matrix);
                    fillLeftBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "3":
                    fillTopRow(matrix);
                    fillRightTop(matrix);
                    fillMediumRow(matrix);
                    fillRightBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "4":
                    fillLeftTop(matrix);
                    fillRightTop(matrix);
                    fillMediumRow(matrix);
                    fillRightBottom(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "5":
                    fillTopRow(matrix);
                    fillLeftTop(matrix);
                    fillMediumRow(matrix);
                    fillRightBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "6":
                    fillTopRow(matrix);
                    fillLeftTop(matrix);
                    fillMediumRow(matrix);
                    fillLeftBottom(matrix);
                    fillRightBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "7":
                    fillTopRow(matrix);
                    fillRightTop(matrix);
                    fillRightBottom(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "8":
                    fillTopRow(matrix);
                    fillLeftTop(matrix);
                    fillRightTop(matrix);
                    fillMediumRow(matrix);
                    fillLeftBottom(matrix);
                    fillRightBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                case "9":
                    fillTopRow(matrix);
                    fillLeftTop(matrix);
                    fillRightTop(matrix);
                    fillMediumRow(matrix);
                    fillRightBottom(matrix);
                    fillBottomRow(matrix);

                    fillEmptyElements(matrix);
                    break;
                default:
                    console.error('Error en appGenerateDigitMatrix');
                    alert($rootScope.lang.globals.error);
            }

            return matrix;
        };
    }
]);


