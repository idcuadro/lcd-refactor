'use strict';

app.controller("indexCtrl", ["$rootScope", "baseUrl",
    function ($rootScope, baseUrl) {

        /* INICIO Funciones para manejo de Idioma */
        $rootScope.lang = {};

        $rootScope.setLang = function (lang) {
            var request = new XMLHttpRequest();

            request.open('GET', baseUrl + 'lang/' + lang, false);
            request.send(null);

            if (request.status === 200) {
                $rootScope.lang = JSON.parse(request.responseText);
            }
        };

        $rootScope.setLang('es.json');
        /* FIN Funciones para manejo de Idioma */

    }
]);