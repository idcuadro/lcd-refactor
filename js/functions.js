
/**
 * Load popover contents.
 * @param {String} content 
 * @param {String} placement
 */
function loadPopoverJquery(content, placement) {
    $('[data-toggle="popover"]').popover({
        content: content,
        html: true,
        placement: placement,
        container: 'body',
        trigger: 'hover click'
    });
}

/**
 * Convert string to array by character " ".
 * @param {String} string
 */
function stringToArrayBySpaces(input) {
    var string = input.split(" ");
    var stringArray = new Array();

    for (var i = 0; i < string.length; i++) {
        stringArray.push(string[i]);
    }

    return stringArray;
}

/**
 * Convert string to array by character " ".
 * @param {String} string
 */
function stringToArrayByCommas(input) {
    var string = input.split(",");
    var stringArray = new Array();

    for (var i = 0; i < string.length; i++) {
        stringArray.push(string[i]);
    }

    return stringArray;
}

/**
 * Convert string to array by each character.
 * @param {String} string
 */
function stringToArrayByCharacter(input) {
    var stringArray = new Array();
    
    for (var i = 0; i < input.length; i++) {
        stringArray.push(input.charAt(i));
    }

    return stringArray;
}